<?xml version='1.0' encoding='utf-8'?>
<gdml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://service-spi.web.cern.ch/service-spi/app/releases/GDML/schema/gdml.xsd">

    <userinfo>
      <!-- <auxiliary auxtype="PrimaryGun" auxvalue="MaxwellianGun;Neutron;300.;600, 600, -30000, 10, 10, -1000" /> -->
      <auxiliary auxtype="PrimaryGun" auxvalue="MPIGun;Neutron;500, 500, -30000, 20, 40, 0" />
    </userinfo>

    <define>
        <constant name="HALFPI" value="pi/2." />
        <constant name="PI" value="1.*pi" />
        <constant name="TWOPI" value="2.*pi" />
        <position name="P_LV_Tube_1" unit="mm" y="-92.4" />
        <position name="P_LV_Tube_2" unit="mm" y="-66.0" />
        <position name="P_LV_Tube_3" unit="mm" y="-39.6" />
        <position name="P_LV_Tube_4" unit="mm" y="-13.2" />
        <position name="P_LV_Tube_5" unit="mm" y="13.2" />
        <position name="P_LV_Tube_6" unit="mm" y="39.6" />
        <position name="P_LV_Tube_7" unit="mm" y="66.0" />
        <position name="P_LV_Tube_8" unit="mm" y="92.4" />
        <position name="P_LV_Module_300_Center" unit="mm" x="150.0" y="105.6" z="-12.7" />
        <position name="P_LV_Module_500_Center" unit="mm" x="250.0" y="105.6" z="-12.7" />
        <rotation name="R_LV_Module" unit="deg" y="90.0" />
        <rotation name="R_LV_SampleStick" unit="deg" x="-90" />
        <rotation name="R_LV_SampleHolder" unit="deg" x="-90" />
    </define>

    <materials>
        <material name="Water">
          <atom value="LiquidWaterH2O_T293.6K.ncmat"/>
          <D value="1"/>
        </material>

        <material name="Cd">
          <atom value="../ncmat/Cd.ncmat"/>
        </material>

        <material name="HeavyWater">
          <atom value="LiquidHeavyWaterD2O_T293.6K.ncmat"/>
          <D value="1"/>
        </material>

        <material name="Vacuum">
          <atom value="../ncmat/vacuum.ncmat"/>
        </material>

        <material name="Air">
          <atom value="../ncmat/ambient_air.ncmat"/>
        </material>

        <material name="Al">
          <atom value="Al_sg225.ncmat"/>
        </material>

        <material name="V">
          <atom value="V_sg229.ncmat;bragg=0"/>
          <D value="1"/>
        </material>

        <material name="SiPower">
          <atom value="../ncmat/Si_640f.ncmat;packfact=0.5"/>
        </material>

        <material name="Al2O3">
          <atom value="Al2O3_sg167_Corundum.ncmat"/>
        </material>

        <material name="G4_He">
            <atom value="../ncmat/He3_Gas_10bar.ncmat" />
        </material>

    </materials>

    <solids>
        <box lunit="mm" name="WorldBox" x="30000.0" y="30000.0" z="65000.0" />
        <!-- Heavy Water -->
        <tube aunit="deg" deltaphi="360.0" lunit="mm" name="SampleStick" rmax="4.4758" rmin="0.0" startphi="0.0" z="14.3376" />
        <!-- Light Water -->
        <!-- <tube aunit="deg" deltaphi="360.0" lunit="mm" name="SampleStick" rmax="4.4758" rmin="0.0" startphi="0.0" z="4.7792" /> -->
        <!-- Vanadium Stick -->
        <!-- <tube aunit="deg" deltaphi="360.0" lunit="mm" name="SampleStick" rmax="3.175" rmin="0.0" startphi="0.0" z="60.0" /> -->
        <!-- Vanadium SampleHolder -->
        <tube aunit="deg" deltaphi="360.0" lunit="mm" name="SampleHolder" rmax="4.7658" rmin="4.4758" startphi="0.0" z="60.0" />
        <box lunit="mm" name="MonitorIn" x="40.0" y="40.0" z="1.0" />
        <box lunit="mm" name="Module300_L" x="25.4" y="211.2" z="300" />
        <box lunit="mm" name="Module300_G" x="300" y="211.2" z="25.4" />
        <box lunit="mm" name="Module500_L" x="25.4" y="211.2" z="500" />
        <box lunit="mm" name="Module500_G" x="500" y="211.2" z="25.4" />
        <tube aunit="deg" deltaphi="360.0" lunit="mm" name="Tube300" rmax="12.7" rmin="0.0" startphi="0.0" z="300.0" />
        <tube aunit="deg" deltaphi="360.0" lunit="mm" name="Tube300_Wall" rmax="12.7" rmin="12.2" startphi="0.0" z="300.0" />
        <tube aunit="deg" deltaphi="360.0" lunit="mm" name="Tube300_He" rmax="12.2" rmin="0.0" startphi="0.0" z="300.0" />
        <tube aunit="deg" deltaphi="360.0" lunit="mm" name="Tube500" rmax="12.7" rmin="0.0" startphi="0.0" z="500.0" />
        <tube aunit="deg" deltaphi="360.0" lunit="mm" name="Tube500_Wall" rmax="12.7" rmin="12.2" startphi="0.0" z="500.0" />
        <tube aunit="deg" deltaphi="360.0" lunit="mm" name="Tube500_He" rmax="12.2" rmin="0.0" startphi="0.0" z="500.0" />
    </solids>

    <structure>
        <volume name="LV_Tube_SampleStick">
            <solidref ref="SampleStick" />
            <materialref ref="HeavyWater" />
            <auxiliary auxtype="Color" auxvalue="#ffe4ff00" />
        </volume>

        <volume name="LV_Tube_SampleHolder">
            <solidref ref="SampleHolder" />
            <materialref ref="V" />
            <auxiliary auxtype="Color" auxvalue="#00ff1700" />
        </volume>

        <volume name="LV_MonitorIn">
            <solidref ref="MonitorIn" />
            <materialref ref="Vacuum" />
            <auxiliary auxtype="Color" auxvalue="#00ff1700" />
        </volume>

        <volume name="LV_Tube300_He">
            <solidref ref="Tube300_He" />
            <materialref ref="G4_He" />
            <auxiliary auxtype="Color" auxvalue="#00ff1700" />
            <auxiliary auxtype="Sensitive" auxvalue="NeutronSq;SofQ_300;0,0,0;0,0,1;30000.;0.1;120;1000;ABSORB"/>
        </volume>

        <volume name="LV_Tube300_Wall">
            <solidref ref="Tube300_Wall" />
            <materialref ref="Al" />
            <auxiliary auxtype="Color" auxvalue="#00ff8400" />
        </volume>

        <volume name="LV_Tube300">
            <solidref ref="Tube300" />
            <materialref ref="Vacuum" />
            <auxiliary auxtype="Color" auxvalue="#ff9f0000" />
            <physvol name="LV_Tube300_Wall">
                <volumeref ref="LV_Tube300_Wall" />
            </physvol>
            <physvol name="LV_Tube300_He">
                <volumeref ref="LV_Tube300_He" />
            </physvol>
        </volume>

        <volume name="LV_Tube500_He">
            <solidref ref="Tube500_He" />
            <auxiliary auxtype="Sensitive" auxvalue="NeutronSq;SofQ_500;0,0,0;0,0,1;30000.;0.1;120;1000;ABSORB"/>
            <materialref ref="G4_He" />
            <auxiliary auxtype="Color" auxvalue="#00ff1700" />
        </volume>

        <volume name="LV_Tube500_Wall">
            <solidref ref="Tube500_Wall" />
            <materialref ref="Al" />
            <auxiliary auxtype="Color" auxvalue="#00ff8400" />
        </volume>

        <volume name="LV_Tube500">
            <solidref ref="Tube500" />
            <materialref ref="Vacuum" />
            <auxiliary auxtype="Color" auxvalue="#ff9f0000" />
            <physvol name="LV_Tube500_Wall">
                <volumeref ref="LV_Tube500_Wall" />
            </physvol>
            <physvol name="LV_Tube500_He">
                <volumeref ref="LV_Tube500_He" />
            </physvol>
        </volume>

        <volume name="LV_Box_Module300_L">
            <solidref ref="Module300_L" />
            <materialref ref="Vacuum" />
            <auxiliary auxtype="Color" auxvalue="#ff9f0000" />
            <auxiliary auxtype="Sensitive" auxvalue="PSD;NeutronHistMapM300;-170;170;68;-125;125;50;YZ"/>
            <physvol name="LV_Tube300_1">
                <volumeref ref="LV_Tube300" />
                <positionref ref="P_LV_Tube_1" />
            </physvol>
            <physvol name="LV_Tube300_2">
                <volumeref ref="LV_Tube300" />
                <positionref ref="P_LV_Tube_2" />
            </physvol>
            <physvol name="LV_Tube300_3">
                <volumeref ref="LV_Tube300" />
                <positionref ref="P_LV_Tube_3" />
            </physvol>
            <physvol name="LV_Tube300_4">
                <volumeref ref="LV_Tube300" />
                <positionref ref="P_LV_Tube_4" />
            </physvol>
            <physvol name="LV_Tube300_5">
                <volumeref ref="LV_Tube300" />
                <positionref ref="P_LV_Tube_5" />
            </physvol>
            <physvol name="LV_Tube300_6">
                <volumeref ref="LV_Tube300" />
                <positionref ref="P_LV_Tube_6" />
            </physvol>
            <physvol name="LV_Tube300_7">
                <volumeref ref="LV_Tube300" />
                <positionref ref="P_LV_Tube_7" />
            </physvol>
            <physvol name="LV_Tube300_8">
                <volumeref ref="LV_Tube300" />
                <positionref ref="P_LV_Tube_8" />
            </physvol>
        </volume>

        <volume name="LV_Box_Module500_L">
            <solidref ref="Module500_L" />
            <materialref ref="Vacuum" />
            <auxiliary auxtype="Color" auxvalue="#ff9f0000" />
            <auxiliary auxtype="Sensitive" auxvalue="PSD;NeutronHistMapM500;-270;270;108;-125;125;50;YZ"/>
            <physvol name="LV_Tube500_1">
                <volumeref ref="LV_Tube500" />
                <positionref ref="P_LV_Tube_1" />
            </physvol>
            <physvol name="LV_Tube500_2">
                <volumeref ref="LV_Tube500" />
                <positionref ref="P_LV_Tube_2" />
            </physvol>
            <physvol name="LV_Tube500_3">
                <volumeref ref="LV_Tube500" />
                <positionref ref="P_LV_Tube_3" />
            </physvol>
            <physvol name="LV_Tube500_4">
                <volumeref ref="LV_Tube500" />
                <positionref ref="P_LV_Tube_4" />
            </physvol>
            <physvol name="LV_Tube500_5">
                <volumeref ref="LV_Tube500" />
                <positionref ref="P_LV_Tube_5" />
            </physvol>
            <physvol name="LV_Tube500_6">
                <volumeref ref="LV_Tube500" />
                <positionref ref="P_LV_Tube_6" />
            </physvol>
            <physvol name="LV_Tube500_7">
                <volumeref ref="LV_Tube500" />
                <positionref ref="P_LV_Tube_7" />
            </physvol>
            <physvol name="LV_Tube500_8">
                <volumeref ref="LV_Tube500" />
                <positionref ref="P_LV_Tube_8" />
            </physvol>
        </volume>

        <volume name="LV_Box_Module300">
            <solidref ref="Module300_G" />
            <materialref ref="Vacuum" />
            <auxiliary auxtype="Color" auxvalue="#ff9f0000" />
            <physvol name="Module300_Local">
                <volumeref ref="LV_Box_Module300_L" />
                <rotationref ref="R_LV_Module" />
            </physvol>
        </volume>

        <volume name="LV_Box_Module500">
            <solidref ref="Module500_G" />
            <materialref ref="Vacuum" />
            <auxiliary auxtype="Color" auxvalue="#ff9f0000" />
            <physvol name="Module500_Local">
                <volumeref ref="LV_Box_Module500_L" />
                <rotationref ref="R_LV_Module" />
            </physvol>
        </volume>

        <volume name="worldVOL">
            <solidref ref="WorldBox" />
            <materialref ref="Vacuum" />
            <auxiliary auxtype="Color" auxvalue="#ff9f0000" />

            <physvol name="LV_Tube_SampleStick">
                <volumeref ref="LV_Tube_SampleStick" />
                <rotationref ref="R_LV_SampleStick" />
            </physvol>

            <physvol name="LV_Tube_SampleHolder">
                <volumeref ref="LV_Tube_SampleHolder" />
                <rotationref ref="R_LV_SampleHolder" />
            </physvol>

            <physvol name="LV_MonitorIn">
                <volumeref ref="LV_MonitorIn" />
                <position name="P_MonitorIn" unit="mm" x="0.0" y="0.0" z="-457.0" />
            </physvol>

            <physvol name="module10203">
                <volumeref ref="LV_Box_Module500" />
                <!-- <position name="P_module10203" unit="mm" x="-513.21952039157" y="-93.80000000000001" z="2936.368359973269" /> -->
                <position name="P_module10203" unit="mm" x="-651.495" y="-93.79999999999995" z="2455.7" />
               <rotation name="R_module10203" unit="deg" x="-0.0" y="75.40589600264069" z="-0.0" />
               <auxiliary auxtype="name" auxvalue="module10203" />
            </physvol>

             <!-- <physvol name="module10303">
                <volumeref ref="LV_Box_Module500" />
                <position name="P_module10303" unit="mm" x="-993.5439465134674" y="-93.79999999999998" z="2041.9928173722174" />
                <rotation name="R_module10303" unit="deg" x="-0.0" y="74.0341369080653" z="-0.0" />
                <auxiliary auxtype="name" auxvalue="module10303" />
            </physvol> -->

            <physvol name="module10701">
                <volumeref ref="LV_Box_Module300" />
                <position name="P_module10701" unit="mm" x="-363.988798" y="-307.79999999999995" z="-1238.990777" />
                <rotation name="R_module10701" unit="deg" x="0.0" y="0.0" z="0.0" />
                <auxiliary auxtype="name" auxvalue="module10701" />
            </physvol>

            <!-- <physvol name="module10801">
                <volumeref ref="LV_Box_Module300" />
                <position name="P_module10801" unit="mm" x="363.988798" y="-307.79999999999995" z="-1238.990777" />
                <rotation name="R_module10801" unit="deg" x="0.0" y="0.0" z="0.0" />
                <auxiliary auxtype="name" auxvalue="module10801" />
            </physvol> -->


            <!-- <physvol name="module10504">
                <volumeref ref="LV_Box_Module500" />
                <position name="P_module10504" unit="mm" x="-1174.8288960730047" y="13.200000000000003" z="-59.71411523061067" />
                <rotation name="R_module10504" unit="deg" x="-0.0" y="-76.393" z="-0.0" />
                <auxiliary auxtype="name" auxvalue="module10504" />

            </physvol> -->


            <!-- <physvol name="module11004">
                <volumeref ref="LV_Box_Module500" />
                <position name="P_module11004" unit="mm" x="1174.8262974655179" y="13.200000000000003" z="-59.71348558252896" />
                <rotation name="R_module11004" unit="deg" x="-0.0" y="76.39300000000004" z="-0.0" />
                <auxiliary auxtype="name" auxvalue="module11004" />
            </physvol> -->
        </volume>
    </structure>

    <setup name="Default" version="1.0">
        <world ref="worldVOL" />
    </setup>
</gdml>
