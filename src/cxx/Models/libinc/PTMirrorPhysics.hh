#ifndef Prompt_MirrorPhyiscs_hh
#define Prompt_MirrorPhyiscs_hh

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  This file is part of Prompt (see https://gitlab.com/xxcai1/Prompt)        //
//                                                                            //
//  Copyright 2021-2022 Prompt developers                                     //
//                                                                            //
//  Licensed under the Apache License, Version 2.0 (the "License");           //
//  you may not use this file except in compliance with the License.          //
//  You may obtain a copy of the License at                                   //
//                                                                            //
//      http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                            //
//  Unless required by applicable law or agreed to in writing, software       //
//  distributed under the License is distributed on an "AS IS" BASIS,         //
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
//  See the License for the specific language governing permissions and       //
//  limitations under the License.                                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#include <string>

#include "PromptCore.hh"
#include "PTLookUpTable.hh"
#include "PTDiscreteModel.hh"

#include "NCrystal/NCrystal.hh"

namespace Prompt {

  class MirrorPhyiscs  : public DiscreteModel {
    public:
      MirrorPhyiscs(double mvalue, double weightCut = 1e-5);
      virtual ~MirrorPhyiscs() override;

      virtual double getCrossSection(double ekin) const override;
      virtual double getCrossSection(double ekin, const Vector &dir) const override;
      virtual void generate(double ekin, const Vector &nDirInLab, double &final_ekin, Vector &reflectionNor, double &scaleWeight) const override;

    private:
      std::shared_ptr<LookUpTable> m_table;
      double m_wcut;
      SingletonPTRand &m_rng;

  };

}

#endif
