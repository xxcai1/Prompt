#ifndef Prompt_MaterialPhysics_hh
#define Prompt_MaterialPhysics_hh

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  This file is part of Prompt (see https://gitlab.com/xxcai1/Prompt)        //
//                                                                            //
//  Copyright 2021-2022 Prompt developers                                     //
//                                                                            //
//  Licensed under the Apache License, Version 2.0 (the "License");           //
//  you may not use this file except in compliance with the License.          //
//  You may obtain a copy of the License at                                   //
//                                                                            //
//      http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                            //
//  Unless required by applicable law or agreed to in writing, software       //
//  distributed under the License is distributed on an "AS IS" BASIS,         //
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
//  See the License for the specific language governing permissions and       //
//  limitations under the License.                                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#include <string>
#include "PromptCore.hh"
#include "PTModelCollection.hh"

namespace Prompt {
  class MaterialPhysics  {
  public:
    MaterialPhysics();
    virtual ~MaterialPhysics();

    double macroCrossSection(double ekin, const Prompt::Vector &dir);
    double sampleStepLength(double ekin, const Prompt::Vector &dir);
    double getScaleWeight(double step, bool selBiase);
    void sampleFinalState(double ekin, const Vector &dir, double &final_ekin, Vector &final_dir, double &scaleWeight);
    void addComposition(const std::string &cfg, double bias=1.0);

  private:
    double calNumDensity(const std::string &cfg);
    SingletonPTRand &m_rng;
    std::unique_ptr<ModelCollection> m_modelcoll;
    double m_numdensity;

  };

}

#endif
